import { StateProvider } from 'apprise-frontend-core/state/provider';
import { MessageBoardContext, initialBoards, initialMessageColours } from './context';
import { useDefaultMessageResolverFactory } from './defaults';
import { MessageZoneProps } from './zone';

import { MessageColourContext } from './context';
import { MessageZone } from './zone';
 
export const Messages = (props: MessageZoneProps) => {

    const { children,  messageResolverFactory = useDefaultMessageResolverFactory, ...rest  } = props

    return <StateProvider initialState={initialMessageColours} context={MessageColourContext}>
        <StateProvider initialState={initialBoards} context={MessageBoardContext}>
            <MessageZone messageResolverFactory={messageResolverFactory} {...rest}>
                {children}
            </MessageZone>
        </StateProvider>
    </StateProvider>


}