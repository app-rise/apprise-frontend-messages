
import { Empty } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { Label } from 'apprise-ui/label/label'
import { useAsyncRunner } from 'apprise-ui/utils/asyncrunner'
import { DownIcon, LoadIcon } from 'apprise-ui/utils/icons'
import debounce from 'lodash/debounce'
import { CSSProperties, PropsWithChildren, ReactText, forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'
import Autosizer from 'react-virtualized-auto-sizer'
import { ListOnItemsRenderedProps, ListOnScrollProps, VariableSizeList } from "react-window"
import { Fragment } from 'react/jsx-runtime'
import "./boardlist.scss"
import { MessageIcon } from './constants'
import { FlowMessage, Message } from './model'

type BoardListProps = {


    messages: FlowMessage[]
    render: (_: FlowMessage) => JSX.Element

    loadMore: () => Promise<FlowMessage[]>
    hasMore: boolean

    onScroll?: (rows: Message[], fullyScrolled: boolean) => void

}


export type BoardListApi = {

    fullyScrolled: () => boolean
    scroll: (index?: number) => void

}

export const BoardList = forwardRef((props: BoardListProps, clientref) => {

    return <div className='message-list'>
        <Autosizer >{size =>
            <Inner ref={clientref} {...props} {...size} />
        }</Autosizer>
    </div>

})


const Inner = forwardRef((props: BoardListProps & {

    height: number
    width: number

}, clientref) => {

    const t = useT()

    const { hasMore, render, loadMore, messages, onScroll, height, width } = props

    const [scrolled, setScrolled] = useState(false)

    const [preview, setPreview] = useState<FlowMessage[]>([])

    const listRef = useRef<VariableSizeList<RowItemData>>(null)

    const fullyScrolled = () => scrolled

    const scroll = (index?: number) => listRef.current?.scrollToItem(index ?? messages.length - 1, 'end')



    useImperativeHandle(clientref, () => ({ fullyScrolled, scroll }))

    // tracks items appended since downloading the first batch. 
    // along with a flag that indicates wheher they have been seen by the user.
    const [unseen, setUnseen] = useState<Record<string, boolean>>({})

    // tracks the last item to recognise when new items are appended to the bottom of the list,
    // so we know we should keep the list scrolled in onItemsRendered().
    const lastKey = useRef<string | number | undefined>(undefined)

    // the heights of each item, measured on-the-fly when previewing each incoming batches.
    const heights = useRef<Record<string, number>>({})

    const firstLoad = useRef(true)

    const { waitFor, running, error } = useAsyncRunner()

    const loadMoreMessages = waitFor(async () => {

        const batch = await loadMore()

        setPreview(batch)

    })
    const readyToLoad = !running && hasMore

    const sumOfHeights = Object.values(heights.current).reduce((a, b) => a + b, 0)
    const currentListHeight = Math.min(height, sumOfHeights)
    const unseenCount = Object.values(unseen).length


    // previews appended items, so correct list height can be recalculated.
    // if list is scrolled, keeps it scrolled so new messages are "seen".
    // if list isn't scrolled, remember there are messsages to "see", so users can be warned. 
    const previewAppendedIfAny = () => {

        // have new elements been appeneded at the bottom? if so schedule to preview them and learn their height.
        const lastIndex = lastKey.current ? messages.findIndex(m => m.id === lastKey.current) : -1
        const appended = lastIndex >= 0 ? messages.slice(lastIndex + 1) : []

        if (appended.length > 0) {

            setPreview(appended)

            // if list is fully scrolled, then keep it scrolled.
            if (scrolled)

                scroll()

            else {

                const tracked = appended.reduce((acc, n) => ({ ...acc, [n.id]: true }), {} as Record<string, boolean>)

                setUnseen(v => ({ ...v, ...tracked }))

            }

        }

        // updates record of last element for future checks.
        lastKey.current = messages[messages.length - 1] ? messages[messages.length - 1].id : undefined
    }

    // loads batches to fill up viewport, then scroll back to start (first load) or to prev. position (on demand).
    const fillViewport = () => {

        // wait for loading to finish, stop trying on error.
        if (running || error)
            return

        const sumOfHeights = Object.values(heights.current).reduce((a, b) => a + b, 0)

        // fill viewport if we have more to load.
        if (sumOfHeights < height && readyToLoad)
            loadMoreMessages()

        else {

            if (firstLoad.current) {

                scroll()
                firstLoad.current = false

            }
            else

                // the previous position is the size of the last batch loaded, 
                // ie. how many we've just previewed.
                listRef.current?.scrollToItem(preview.length, 'start')

        }


        // eslint-disable-next-line
    }


    useEffect(previewAppendedIfAny)
    useEffect(fillViewport)



    // passed on to items to report height after rendering.
    const setHeight = (key, s) => {

        heights.current[key] = s
        listRef.current?.resetAfterIndex(0, false)
    }



    const onListScroll = debounce(({ scrollDirection, scrollOffset }: ListOnScrollProps) => {

        if (scrollDirection === 'backward' && scrollOffset === 0 && readyToLoad)
            loadMoreMessages()

    })


    const onMessageRendered = (props: ListOnItemsRenderedProps) => {

        const { visibleStartIndex, visibleStopIndex, overscanStopIndex } = props

        // it's fully scrolled if all messages are visible. 
        const fullyScrolled = visibleStopIndex === overscanStopIndex

        setScrolled(fullyScrolled)

        const showing = messages.slice(visibleStartIndex, visibleStopIndex + 1)

        // untrack items in view
        showing.forEach(m => delete unseen[m.id])

        setUnseen(unseen)

        // call back client.
        onScroll?.(showing, fullyScrolled)

    }


    // avoid flashes as it's loading.
    const isLoaded = running === false  // explict, as undefined and true are both possibilities.

    const empty = isLoaded && messages.length === 0 && !hasMore


    // console.log({ height, width, messages, preview, heights: heights.current, unseen: Object.keys(unseen), readyToLoad, scrollable, currentListHeight })

    const scrollDownBtn = <Button noLabel icon={<DownIcon />} noReadonly className={classname('indicator', 'scroll-indicator', isLoaded && !scrolled && 'active')}
        tip={t('message.scroll_to_bottom')} tipDelay={1} tipPlacement='bottomRight'
        onClick={() => scroll()} />

    const unseenMsgBtn = <Button noLabel noReadonly icon={<MessageIcon />} className={classname('indicator', 'unseen-indicator', isLoaded && !!unseenCount && 'active')}
        tip={t('message.new_messages', { count: unseenCount })} tipPlacement='bottomRight'
        onClick={() => scroll()} />




    // renders latest batch in zoer-height containers 0 to learn message heights. 
    const hiddenPreview = < div style={{ height: 0, overflow: 'hidden' }} > {

        preview.map((t, index) => <HeightReporting key={t.id} id={t.id} setHeight={setHeight} >
            {render(preview[index])}
        </HeightReporting>
        )
    }
    </div >


    return <div className='list-contents' style={{ height, width }}>

        <Label className={classname('batch-loader', running && 'active')} icon={<LoadIcon />} title={t('message.loading_more')} />


        {empty ?

            <Empty className='nodata' description={t('message.no_messages')} image={Empty.PRESENTED_IMAGE_SIMPLE} />

            :

            <Fragment>

                {scrollDownBtn}
                {unseenMsgBtn}

                {hiddenPreview}


                <VariableSizeList<RowItemData> ref={listRef} height={currentListHeight} width={width}
                    onScroll={onListScroll}
                    onItemsRendered={onMessageRendered}
                    itemCount={messages.length}

                    // react-window passes this to RowItems.
                    itemData={{ messages, render, setHeight }}

                    itemKey={i => messages[i].id}
                    itemSize={i => messages[i] ? heights.current[(messages[i].id)] ?? 0 : 0}>

                    {RowItem}

                </VariableSizeList>

            </Fragment>

        }

    </div>


})




// wraps the childrento measure their height on mount.
const HeightReporting = (props: PropsWithChildren<{ id: string, setHeight: (index: ReactText, _: number) => void }>) => {

    const { id, children, setHeight } = props

    const ref = useRef<HTMLDivElement>(null)

    // eslint-disable-next-line
    useEffect(() => setHeight(id, ref.current?.getBoundingClientRect().height ?? 0), [])

    return <div ref={ref}>
        {children}
    </div>
}


type RowItemData = {

    messages: FlowMessage[]
    render: (_: FlowMessage) => JSX.Element
    setHeight: (key: number | string, _: number) => void
}


// wraps a data item as a row in the react-window's table, which wants to apply styles with absolute positioning.
// the 'data' property is react-window's idiomatic way to propagate data and functions required for rendering and
// provided in the 'itemData' property.

const RowItem = (props: { index: number, style?: CSSProperties, data: RowItemData }) => {

    const { index, style, data } = props

    const { messages, setHeight, render } = data

    const message = messages[index]

    return <div style={style}>
        <HeightReporting id={message.id} setHeight={setHeight}>
            {render(message)}
        </HeightReporting>
    </div>
}


