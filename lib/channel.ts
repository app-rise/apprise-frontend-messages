
import { Channel, Message } from './model'
import { useMessageTopics } from './topic'

export const useMessageChannel = () => {

    const topics = useMessageTopics()

    const self = {


        refOf: (c:Channel) => `read: ${c.read.map(topics.refOf)} (${c.mode}), write: ${c.write?.map(topics.refOf) ?? 'none'}`,

        // a message matches a channel if it has all the read topics of the channel.
        // the match is 'strict' if it has only and only those topics. 
        matches: (m: Message, { read, mode = 'lax' }: Channel) => {

            const { matched, extra } = topics.partitionTopics(m.topics, read)

            return matched.length === read.length && (mode === 'lax' || extra.length === 0)

        }
    }

    return self;

}