

import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { Label } from 'apprise-ui/label/label'
import { TimeLabel } from 'apprise-ui/time/label'
import { Tip } from 'apprise-ui/tooltip/tip'
import { useAsyncRunner } from 'apprise-ui/utils/asyncrunner'
import { DotIcon } from 'apprise-ui/utils/icons'
import { PropsWithChildren } from 'react'
import { AiOutlineDoubleRight } from 'react-icons/ai'
import './boardmessage.scss'
import { MessageReferenceViewer } from './boardreference'
import { useMessageFlow } from './flow'
import { useMessage } from './message'
import { FlowMessage, MessageInfo, Topic, TopicInfo } from './model'
import { useMessageTopics } from './topic'
import { useMessageZone } from './zone'

export type BoardMessageProps = {

    message: FlowMessage

    topicFilter?: (_: Topic, __: MessageInfo) => boolean
    
    onRead: (m: FlowMessage, flag: boolean) => Promise<void>
    
    noReply?: boolean

    onReply: (m: FlowMessage) => void
    

}

export const BoardMessage = (props: BoardMessageProps) => {

    const t = useT()

    const { message, onRead, noReply, onReply, topicFilter = () => true } = props


    const { messageResolver } = useMessageZone()

    const info = messageResolver(message)

    const { Avatar, read, own } = info

    const messages = useMessage()
    const flow = useMessageFlow()
    const topics = useMessageTopics()

    const { running, waitFor, PulsingDotIcon } = useAsyncRunner()

    const readMessage = waitFor(onRead)

    const subtopics = flow.subtopics(message).map(t => ({ ...t, ...topics.resolveTopic(t) }) as Topic & TopicInfo).filter(t => t && topicFilter(t, info))

    return <div className={classname('board-message', read ? 'read-message' : 'unread-message', own ? 'own-message' : 'other-message', message.replies && 'message-with-reply')}>

        <div className="message-sidebar">
            <Avatar info={info} />
        </div>

        <div className="message-main">

            {subtopics.length > 0 &&

                <TopicRow info={info} topics={subtopics}>
                    <UnreadLabel read={read} />
                </TopicRow>

            }

            <MetadataRow info={info}>
                {subtopics.length > 0 ||
                    <UnreadLabel read={read} />
                }
            </MetadataRow>


            <div className='message-content'>
                {message.replies && <MessageReferenceViewer className='message-content-reply' messageRef={message.replies} />}
                {messages.contentOf(message)}
            </div>

            <div className="message-bottombar">
                {own ||

                    <Button size='small' iconPlacement='left' type='ghost' noReadonly
                        icon={<PulsingDotIcon />}

                        className={classname('message-action', running ? 'action-loading' : read && 'action-unread')}

                        onClick={() => readMessage(message, !read)}>
                        {t(read ? 'message.mark_unread' : 'message.mark_read')}
                    </Button>

                }

                {noReply ||

                    <Button size='small' iconPlacement='left' type='ghost'
                        icon={<AiOutlineDoubleRight />}
                        className='message-action'
                        onClick={() => onReply(message)}>
                        {t('message.reply')}
                    </Button>


                }


            </div>

        </div>


    </div>


}

const UnreadLabel = (props: { read: boolean }) => {

    const t = useT()

    const { read } = props

    if (read)
        return null

    return <Tip tip={t('message.unread_message')}>
        <DotIcon style={{ marginRight: 10 }} size='20' color='lightseagreen' />
    </Tip>
}


const TopicRow = (props: PropsWithChildren<{

    info: MessageInfo
    topics: TopicInfo[]

}>) => {

    const t = useT()

    const { topics, children } = props

    if (!topics.length)
        return null

    return <div className='message-topic-row'>

        {children}

        {topics.map((resolved, index) =>

            <Label key={index} noIcon linkTo={resolved.link} className="message-topic" title={resolved.label}
                tip={resolved.tip && <div style={{ display: 'flex', alignItems: 'center' }}>{t('message.topic_tip_prefix')}&nbsp;{resolved.tip}</div>} />

        )

        }</div>


}

const MetadataRow = (props: PropsWithChildren<{

    info: MessageInfo


}>) => {

    const { children, info } = props


    return <div className='message-metadata-row'>
        {children}
        <span className="message-author">{info.authorName}</span>
        <TimeLabel noIcon noTip className="message-date" date={new Date(info.date)} />

    </div>
}