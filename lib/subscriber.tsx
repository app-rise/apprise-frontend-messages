import { EventSubscriber } from 'apprise-frontend-events/subscriptions'
import { PropsWithChildren } from 'react'
import { anyRecipient } from './constants'
import { Message } from './model'
import { useBoardStore } from './store'

export type MessageEvent =  {

    type: 'added' | 'updated'
    message: Message
}


export type MessageSubscriberProps = {

    name?: string
    recipients?: string[]

}

export const MessageSubscriber = (props: PropsWithChildren<MessageSubscriberProps>) => {

    const store = useBoardStore()

    const { name, recipients = [anyRecipient], children } = props

    const onEvent = ({message, type}:MessageEvent) => {

        switch (type) {
            
            case "added":  store.addOne(message); break;
            case "updated": store.updateOne(message); break;
            default:
                console.error("unknown message event type", type)
        }

    }

    const subcriptions = recipients.map(topic => ({ topic: `${topic}.message`, onEvent }))

    return <EventSubscriber name={name} subscriptions={subcriptions}>
        {children}
    </EventSubscriber>

}


