import { Avatar } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { Label, LabelProps } from 'apprise-ui/label/label'
import { Tip } from 'apprise-ui/tooltip/tip'
import { CSSProperties, ReactNode } from 'react'
import { FaUserCircle } from 'react-icons/fa'
import { SystemIcon, systemAuthor } from './constants'
import { AvatarProps, MessageResolver } from './model'
import { useAuthorInfo, useMessageColours } from './utils'


export const useDefaultMessageResolverFactory = (): MessageResolver => {

    const t = useT()

    const thisAuthoringParty = useAuthorInfo().party()

    return m => {
      
        const author = m.author ?? systemAuthor
        const authorName = author === systemAuthor ? t('message.system_author') : author
        const AuthorLabel = (props: LabelProps) => <Label icon={<FaUserCircle />} title={authorName} {...props} />

        const authoringParty = m.authoringParty ?? author
        const authoringPartyName = m.authoringParty ? authoringParty : authorName
       
        const own = authoringParty === thisAuthoringParty
        const read = m.authoringParty === thisAuthoringParty || m.readBy.includes(thisAuthoringParty)

        return { ...m, author, authorName, read, own, authoringParty, authoringPartyName, AuthorLabel, Avatar: DefaultAvatar }

    }
}

export const DefaultAvatar = (props: AvatarProps) => {

    const colours = useMessageColours()

    const { info } = props

    const { authoringParty, authoringPartyName } = info

    let icon: ReactNode
    let style: CSSProperties

    switch (info.author) {

        case systemAuthor:
            icon = <SystemIcon style={{ marginTop: 6 }} color='darkslateblue' />
            style = { background: 'lightgray' }
            break;

        default:
            icon = undefined
            style = {
                background: colours.colourOf(authoringParty)
            }

    }


    return <Tip tip={authoringParty}>
        <Avatar icon={icon} style={style}>
            {authoringPartyName[0]?.toUpperCase()}
        </Avatar>
    </Tip>

}