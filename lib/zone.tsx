import { StateProvider } from 'apprise-frontend-core/state/provider'
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard'
import { Fragment, PropsWithChildren, useContext } from 'react'
import { MessageAuthoringContext, MessageZoneContext } from './context'
import { MessageResolver, RouteResolver, TopicResolver } from './model'


import { utils } from 'apprise-frontend-core/utils/common'
import { MessageSubscriber, MessageSubscriberProps } from './subscriber'


export const useMessageZone = () => {

    const state = useContext(MessageZoneContext)

    const self = {

        messageResolver: state.get().messageResolver,


        registerInfoProducer: (producer: MessageResolver) => state.setQuietly(s => s.messageResolver = producer),


        addTopicResolver: (resolver: TopicResolver) => {

            state.setQuietly(s => s.topicResolvers[resolver.type] = resolver)

            return self

        },

        registerTopicResolvers: (resolvers: TopicResolver[]) => {

            state.setQuietly(s => s.topicResolvers = utils().index(resolvers).by(r => r.type))

            return self

        },

        lookupTopicResolver: (type: string) => state.get().topicResolvers[type],

        addRouteResolver: (resolver: RouteResolver) => {

            state.setQuietly(s => s.routeResolvers.push(resolver))

            return self
        },

        registerRouteResolvers: (resolvers: RouteResolver[]) => {

            state.setQuietly(s => s.routeResolvers = resolvers)

            return self

        },

        routeResolvers: () => state.get().routeResolvers
    }

    return self;

}

export type MessageZoneProps = PropsWithChildren<Partial<MessageSubscriberProps & {

    authoringParty: string
    author: string
    topicResolverFactory: () => TopicResolver[]
    routerResolverFactory: () => RouteResolver[]
    messageResolverFactory: () => MessageResolver

}>>



export const MessageZone = (props: MessageZoneProps) => {

    const parentRegistry = useContext(MessageZoneContext).get()

    const { children, authoringParty, author, name = 'global zone', recipients, ...rest } = props

    let inner = <ZoneInitialiser {...rest}>
        {children}
    </ZoneInitialiser>

    // if provided, this overrides any authoring party that might have been set at parent level
    if (authoringParty || author)
        inner = <StateProvider initialState={{ author, authoringParty }} context={MessageAuthoringContext} >
            {inner}
        </StateProvider>


    return <StateProvider initialState={parentRegistry} context={MessageZoneContext}>
        <MessageSubscriber name={name} recipients={recipients}>
            {inner}
        </MessageSubscriber>
    </StateProvider>

}



export const ZoneInitialiser = (props: MessageZoneProps) => {

    const registry = useMessageZone()

    const { children, messageResolverFactory, topicResolverFactory, routerResolverFactory } = props

    const infoProducer = messageResolverFactory?.()
    const topicResolvers = topicResolverFactory?.()
    const routeResolvers = routerResolverFactory?.()

    const activate = async () => {

        if (infoProducer)
            registry.registerInfoProducer(infoProducer)

        if (topicResolvers)
            registry.registerTopicResolvers(topicResolvers)

        if (routeResolvers)
            registry.registerRouteResolvers(routeResolvers)
    }

    const { content } = useRenderGuard({

        render: <Fragment>
            {children}
        </Fragment>,

        orRun: activate
    })

    return content

}


