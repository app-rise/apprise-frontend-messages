import { State, fallbackStateOver } from 'apprise-frontend-core/state/api';
import { createContext } from 'react';
import { systemAuthor } from './constants';
import { Flow, FlowMessage, MessageResolver, RouteResolver, TopicResolver } from './model';



export type MessageZoneState = {

    messageResolver: MessageResolver
    topicResolvers: { [type: string]: TopicResolver }
    routeResolvers: RouteResolver[]
}

export const initialMessageZone: MessageZoneState = {

    messageResolver: undefined!,
    topicResolvers: {},
    routeResolvers: []

}


export const MessageZoneContext = createContext<State<MessageZoneState>>(fallbackStateOver(initialMessageZone))


 
export type MessageAuthoringContextState = {

    author: string | undefined
    authoringParty: string | undefined

}

export const initialAuthoringContext: MessageAuthoringContextState = {

    author: undefined,
    authoringParty: undefined


}

export const MessageAuthoringContext = createContext<State<MessageAuthoringContextState>>(fallbackStateOver(initialAuthoringContext))


export type BoardsState = {

    all: Record<string, BoardState>                     // indexed by flow name.

}

export type BoardState = {

    flow: Flow
    unreadCount: number,
    active: boolean                                        // there's a board for it on screen.
    messages: FlowMessage[]
}

export const initialBoards = {
    all: {}
}

export const MessageBoardContext = createContext<State<BoardsState>>(fallbackStateOver(initialBoards))


export type MessageColourState = {

    colours: Record<string, string>

}

export const initialMessageColours: MessageColourState = {

    colours: {

        [systemAuthor]: 'lightgray'


    }
}


export const MessageColourContext = createContext<State<MessageColourState>>(fallbackStateOver(initialMessageColours))
