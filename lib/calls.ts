import { useCall } from 'apprise-frontend-core/client/call'
import { Flow, Message } from './model'

export type MessageBatchFilter = {

    size?: number
    cursor?: string
    flow: Flow


}

export type MessageFilter = {

    scope: string


}

export type MessageBatch = {

    messages: Message[]
    cursor: string

}


const externFlow = (flow: Flow) => (

    {
        scope: flow.scope,
        matchers: flow.channels.map(c =>
            ({ topics: c.channel.read, mode: c.channel.mode })
        )
    }

)

const messageApi = "/message"

export const useMessageCalls = () => {

    const call = useCall()



    const self = {


        getBatch: (filter: MessageBatchFilter) => call.at(`${messageApi}/search`).post<MessageBatch>({

            ...externFlow(filter.flow),

            size: filter.size,
            cursor: filter.cursor

        })

        ,

        removeMany: (filter: MessageFilter ) => call.at(`${messageApi}/remove`).post(filter)

        ,



        markRead: (flow: Flow,  reader: string) => call.at(`${messageApi}/read`).post<number>(

            {

                ...externFlow(flow),
                reader

            }
        )
        
        ,


        getUnreadCount: (flow: Flow,  reader: string) => call.at(`${messageApi}/count`).post<number>(

            {

                ...externFlow(flow),
                reader

            }
        ),

        updateMessage: (message: Message) => call.at(`${messageApi}/${message.id}`).put<Message>(message)
        
        ,


        postMessage: (message: Message) => call.at(messageApi).post<Message>(message)

    }

    return self


}