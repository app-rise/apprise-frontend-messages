import { HiDesktopComputer } from 'react-icons/hi'
import { TbMessage } from 'react-icons/tb'

export const messageType = 'message'

export const messageSingular = 'message.singular'
export const messagePlural = 'message.plural'

export const MessageIcon = TbMessage

export const systemAuthor = "system"

export const defaultBatchSize = 10

export const SystemIcon = HiDesktopComputer

export const anyRecipient = 'any'

export const recipientSeparator = ':'