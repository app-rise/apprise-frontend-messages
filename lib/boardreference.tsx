
import './boardreference.scss'
import { FlowMessage, MessageReference } from './model'
import { useMessage } from './message'
import { useMessageZone } from './zone'





export const MessageReferenceViewer = (props: {

    className?: string
    messageRef: MessageReference

    actions?: React.ReactNode[]

}) => {

    const { className = '', messageRef, actions } = props

    const msg = useMessage()

    const refAsMessage = {...messageRef, topics:[], readBy:[], date: '', flow: undefined!, channel:undefined! } as FlowMessage

    const { AuthorLabel } = useMessageZone().messageResolver(refAsMessage)

    return <div className={`message-reply ${className}`}>
        <div className='reply-main'>
            <div className='reply-author'>
                <span className="message-author">
                    <AuthorLabel />
                </span>
            </div>
            <div className='reply-content'>{msg.contentOf(refAsMessage)}</div>
        </div>
        {actions && 
            <div className="reply-actions">{actions}</div>
        }
    </div>
}