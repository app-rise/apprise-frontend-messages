import { useT } from 'apprise-frontend-core/intl/language'
import { useFeedback } from 'apprise-ui/utils/feedback'
import { useContext } from 'react'
import { useMessageCalls } from './calls'
import { MessageIcon, recipientSeparator } from './constants'
import { MessageBoardContext, MessageZoneContext } from './context'
import { useMessageFlow } from './flow'
import { useMessage } from './message'
import { Flow, FlowMessage, Message, MessageDynamics } from './model'
import { useAuthorInfo } from './utils'
import { useMessageZone } from './zone'



export const useBoardStore = () => {

    const t = useT()

    const fb = useFeedback()

    const state = useContext(MessageBoardContext)

    const registry = useContext(MessageZoneContext)

    const authoring = useAuthorInfo()

    const zone = useMessageZone()

    const calls = useMessageCalls()

    const flows = useMessageFlow()

    const msg = useMessage()


    const self = {

        all: () => state.get().all

        ,

        lookup: (name: string) => self.all()[name]


        ,

        open: async (flow: Flow) => {

            console.log("opening board for", flow.name)

            const unreadCount =  await calls.getUnreadCount(flow, authoring.party())

            state.set(s =>

                s.all[flow.name] = {

                    ...s.all[flow.name],
                    flow,
                    unreadCount,
                    active: true,
                    messages: []
                })

        }

        ,

        close: (flow: Flow) => {

            // keep track to preserve after closing.
            const flowstate = self.lookup(flow.name)

            // do nothing if there's no flow to close.
            if (!flowstate)
                return

            console.log("closing board for", flow.name)

            state.set(s => delete s.all[flow.name])


        }

        ,

        addOne: async (message: Message) => {

            // dispatch message to active matching message flows.
            state.set(s => {
           
                Object.values(s.all).forEach(f => {

                    if (flows.matches(f.flow, message)) {

                        // tries to handle race conditions: ensures notifications are after most recent message downloaded.
                        // also drops notifications if no message has been downloaded yet (null date is now).
                        if (new Date(message.date).getTime() <= new Date(f.messages[0]?.date).getDate() )
                            return

                        const interned = flows.intern(f.flow,message)

                        console.log(`pushed messsage to flow ${f.flow.name} (${authoring.party()})`)

                        // if board is showing, add one notification to it.
                        if (f.active)
                            f.messages.push(interned)

                        // even if the board isn't show, the unread count may still do and need bumping.
                        if (!message.readBy.includes(authoring.party()))
                            f.unreadCount++
                    }

                })
            })

            let route: any

            try {

                route = await registry.get().routeResolvers.map(r => r.routeFor(message)).find(r => r !== undefined)

                if (!route)
                    return

                console.log(`routing message notification to ${route.route}`)

                const notificationMsg = t('message.notify_new', { sender: /* authorInfo.tenantName */ message.author })

                fb.showNotification(notificationMsg, {
                    icon: <MessageIcon />,
                    duration: 4000,
                    route: route.route
                })

            }
            finally {

                if (!route)
                    console.log('silencing unroutable message notification')

            }
        }

        ,

        updateOne: (message: Message) => {

            state.set(s => {

                Object.keys(s.all).forEach(name => {

                    const f = s.all[name]

                    if (flows.matches(f.flow, message)) {

                        const interned = flows.intern(f.flow, message)

                        if (f.active) {

                            const match = f.messages?.findIndex(m => m.id === message.id)

                            // if we have seen the original message, replace it. otherwise append it.
                            if (match >= 0)
                                f.messages[match] = interned
                            else
                                f.messages.push(interned)

                        }

                        // in all cases, refresh unread count.

                        const { own } = zone.messageResolver(message as FlowMessage)

                        if (!own)
                            self.unreadCount(f.flow, name)




                    }

                })
            })

        },



        readOne : async (flow: Flow, m: FlowMessage, value: boolean) => {

            const party = authoring.party();

            const readBy = m.readBy.filter(t => t !== party) ?? []

            if (value)
                readBy.push(party)

            const marked =  flows.intern(flow, await calls.updateMessage({ ...m, readBy }))

            state.set( s => {

                const data =  s.all[flow.name]

                data.messages = data.messages.map(mm => mm.id === marked.id ? marked : mm)
                data.unreadCount = value ? data.unreadCount - 1 : data.unreadCount + 1 
            }) 

        }

        ,

        postOne : async (flow: Flow, text: string, reply: FlowMessage | undefined, dynamics: MessageDynamics) => {

            // note: if required, this must evolve to allow posting to any channel. user needs to select it then.
            const target = reply ? reply.channel : flow.channels.find(c => !c.channel.noPost)?.channel
    
            if (!target)
                throw Error("cannot post to flow " + JSON.stringify(flow))
    
            // use the target channel topics, or if we're reply with the original topics, and finally fallbackl to the read topics.
            const topics = [...target.write ?? (reply ? reply.topics : target.read), ...dynamics.topics ?? []]
    
            const recipient = dynamics.recipients.length ? dynamics.recipients.join(recipientSeparator) : flow.recipient ?? reply?.recipient
    
            let message = { ...msg.makeNew(text, topics, authoring.author()), recipient, scope: flow.scope, replies: msg.refOf(reply) } as Message
    
            if (target.onReply)
                message = target.onReply(message)

            console.log(`posting message from ${message.author ?? 'the system'}...`, message)

    
            const posted = flows.intern(flow, await calls.postMessage(message))
    
            // if this is a reply of an unread message, consider the reply read.
            if (reply) {
    
                const { read } = zone.messageResolver(reply)
    
                if (!read)
                    await self.readOne(flow, reply, true)
    
                
            }
    
            state.set(s => s.all[flow.name].messages.push(posted))
    
            return posted   // any post-processing -- eg. send an email -- is on the client.
    
    
        }

        ,

        markAllRead: async(flow: Flow) => {

            const party = authoring.party()

            await calls.markRead(flow, party)

            state.set(s => s.all[flow.name] = {

                ...s.all[flow.name],

                messages:
                    s.all[flow.name]?.messages?.map(m =>

                        m.readBy.includes(party) ? m : { ...m, readBy: [...m.readBy ?? [], party] }


                    ) ?? [],
                unreadCount: 0
            })


        }


        ,

        unreadCount: async (flow: Flow, name: string = flow.name, force?: 'force'): Promise<number> => {

            const flowstate = self.lookup(name)

            if (!force && flowstate?.unreadCount !== undefined)
                return flowstate.unreadCount

            const count = await calls.getUnreadCount(flow, authoring.party())

            state.set(s => s.all[name] = { ...s.all[name] ?? {}, unreadCount: count })

            return count

        }



    }


    return self


}
