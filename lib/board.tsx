import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { TextBoxApi } from 'apprise-ui/textbox/textbox'
import { useAsyncTask } from 'apprise-ui/utils/asynctask'
import { MutableRefObject, cloneElement, useContext, useEffect, useRef, useState } from 'react'
import { MdOutlineMoreVert } from 'react-icons/md'
import './board.scss'
import { BoardEditor, DynamicsEditor } from './boardeditor'
import { BoardList, BoardListApi } from './boardlist'
import { BoardMessage, BoardMessageProps } from './boardmessage'
import { useMessageCalls } from './calls'
import { defaultBatchSize } from './constants'
import { MessageBoardContext } from './context'
import { useMessageFlow } from './flow'
import { useMessage } from './message'
import { Flow, FlowMessage, Message, MessageDynamics } from './model'
import { useBoardStore } from './store'




const noFlow: Flow = {

    name: 'none',
    channels: []
}

type MessageBoarxProps = Partial<{

    flow: Flow

    batchSize: number

    filter: (_: Message) => boolean

    filterTopics: BoardMessageProps['topicFilter']

    collapsed: boolean | string

    actions: JSX.Element[]

    onPost: (_: FlowMessage) => void

    dynamicsEditor: DynamicsEditor

}>

// display the messages in a given flow in a backward-scrolling infinite list.
export const MessageBoard = (props: MessageBoarxProps) => {

    const t = useT()

    const { flow = noFlow, filterTopics = () => true, actions = [], collapsed, dynamicsEditor } = props

    const list = useRef<BoardListApi>()

    const editor = useRef<TextBoxApi>(undefined!)

    const [actionActive, setActionActive] = useState(false)

    const canEdit = flow.channels.some(c => !c.channel.noPost || !c.channel.noReply)

     const messages = useMessages({ editor, list, ...props })

    return <div className='message-board' >

        <div className='messages'>

            <BoardList ref={list}

                messages={messages.all ?? []}

                hasMore={messages.all === undefined || !!messages.cursor} loadMore={messages.nextBatch}

                render={m =>

                    <BoardMessage message={m} onRead={messages.read} onReply={messages.replyTo} topicFilter={filterTopics} />

                } />

        </div>

        <div className={classname('board-actions', !!actions.length && actionActive && 'active')}>

            {actions.map((a,i) => cloneElement(a,{key:i, noReadonly: true}))}

        </div>

        <div className='board-bottom-bar'>

            {canEdit &&

                <BoardEditor ref={editor} collapsed={!!collapsed} onPost={messages.post} replies={messages.reply} onReset={messages.resetReply}
                    collapsedMessage={typeof collapsed == 'string' ? collapsed : undefined}
                    dynamicTopicSelector={dynamicsEditor} />

            }

            {!!actions.length &&

                <div className='board-action-trigger'>
                    <Button noReadonly tipDelay={1} tipPlacement='topLeft' tip={t('message.action_trigger_tooltip')} type={actionActive ? 'primary' : 'normal'} onClick={() => setActionActive(s => !s)} noLabel iconPlacement='left' icon={<MdOutlineMoreVert size={18} />} />
                </div>
            }


        </div>


    </div>

}


const useMessages = (props: MessageBoarxProps & {

    editor: MutableRefObject<TextBoxApi | undefined>
    list: MutableRefObject<BoardListApi | undefined>
  
}) => {

    const task = useAsyncTask()

    const state = useContext(MessageBoardContext)

    const { flow = noFlow, batchSize = defaultBatchSize, onPost, filter = () => true, editor, list } = props

    const calls = useMessageCalls()

    const msg = useMessage()

    const { intern } = useMessageFlow()

    const store = useBoardStore()
    
    const all = state.get().all[flow.name]?.messages ? [...state.get().all[flow.name].messages].sort(msg.comparator) : undefined
   
    const [reply, setReply] = useState<FlowMessage | undefined>()

    const [cursor, setCursor] = useState<string|undefined>(undefined) 

    // registers board with store, so that can received notifications as they arrive.
    useEffect(() => {

        store. open(flow)
 
         return () => store.close(flow)
 
         // eslint-disable-next-line
     }, [])
     
    const nextBatch = task.make(async () => {

        const { cursor: newcursor, messages = [] } = await calls.getBatch({ flow, cursor, size: batchSize })

        const internedAndFiltered = messages.map(m => intern(flow, m)).filter(filter)

        state.set( s => s.all[flow.name].messages.push(...internedAndFiltered))
 
        setCursor(newcursor)

        return internedAndFiltered.reverse()

    }).with($ => $.noBusyWait().minimumDuration(300)).done()


    const read = async (m: FlowMessage, value: boolean) => store.readOne(flow, m, value)


    const post = async (text: string, dynamics: MessageDynamics) => {

        list.current?.scroll()

        const posted = await store.postOne(flow,text,reply,dynamics)

        
        onPost?.(posted)

    }

    const replyTo = async (m: FlowMessage) => {

        setReply(m)

        editor.current?.focus()

        list.current?.scroll()

    }

    const resetReply = () => setReply(undefined)

    
    return { cursor, all, nextBatch, read, post, reply, replyTo, resetReply }
}
