

import { InputRef } from 'antd'
import { useT } from 'apprise-frontend-core/intl/language'
import { useStable } from 'apprise-frontend-core/utils/function'
import { Button } from 'apprise-ui/button/button'
import { classname } from 'apprise-ui/component/model'
import { ReadonlyContext } from 'apprise-ui/readonly/readonly'
import { TextBox, TextBoxApi } from 'apprise-ui/textbox/textbox'
import { EditIcon, LoadIcon, RemoveItemIcon, UploadIcon } from 'apprise-ui/utils/icons'
import { ForwardedRef, Fragment, MutableRefObject, forwardRef, useContext, useEffect, useImperativeHandle, useRef, useState } from 'react'
import "./boardeditor.scss"
import { MessageReferenceViewer } from './boardreference'
import { FlowMessage, MessageDynamics } from './model'





export type DynamicEditorProps = {

    dynamics: MessageDynamics
    onChange: (_: MessageDynamics) => void
}

export type DynamicsEditor = React.FC<DynamicEditorProps>

const DefaultDynamicsEditor: DynamicsEditor = () => <Fragment />



const initialEditorState = { posting: false, text: undefined as string | undefined }

export const BoardEditor = forwardRef((props: {

    replies?: FlowMessage


    onPost: (_: string, __: MessageDynamics) => Promise<void>
    onReset?: () => void

    dynamicTopicSelector?: DynamicsEditor

    postLabel?: string
    collapsed?: boolean
    collapsedMessage?: string

}, clientref: ForwardedRef<TextBoxApi>) => {

    const t = useT()

    const ref = useRef<InputRef>(null)

    const { onPost, onReset, postLabel, replies, collapsed, collapsedMessage, dynamicTopicSelector } = props

    const readonly = useContext(ReadonlyContext)

    const [visible, setVisible] = useState(!collapsed)

    const [state, setState] = useState(initialEditorState)

    const { posting, text } = state

    const { EditorDynamics, dynamics } = useEditorDynamics({ visible, dynamicTopicSelector })

    useImperativeHandle(clientref, () => ({

        focus: () => ref.current?.focus()

    }))

    const post = async () => {

        ref.current?.focus()

        setState(s => ({ ...s, posting: true }))

        try {

            await onPost(text!, dynamics)

        }

        finally {

            if (!replies && collapsed)
                setVisible(false)

            setState(initialEditorState)

            onReset?.()
        }


    }

    const placeholder = t('message.editor_placeholder')

    const mutableref = clientref as MutableRefObject<InputRef>

    useEffect(() => {

        if (collapsed && visible)
            mutableref?.current.focus()

    }, [collapsed, visible, mutableref])

    return <div className={classname('board-editor', visible || 'editor-placeholder')}> {

        visible ?

            <Fragment>

                <div className='editor-content'>

                    {replies &&

                        <MessageReferenceViewer className='editor-reply' messageRef={replies}  actions={[
                                <Button noLabel icon={<RemoveItemIcon color='coral' />} className='reply-clear' type='ghost' key='clear' noReadonly onClick={onReset} />
                            ]} />

                    }

                    <EditorDynamics />

                    
                    {/* <TextArea value={text} disabled={readonly} autoSize={{minRows:1, maxRows:5}} className='editor-input' placeholder={placeholder}
                        onChange={text => setState(s => ({ ...s, text: text.target.value }))} /> */}

                    <TextBox disabled={readonly} multi={{ start: 1, end: 5 }} ref={ref} className='editor-input' placeholder={placeholder}
                        onChange={text => setState(s => ({ ...s, text }))}>
                        {text}
                    </TextBox>

                </div>

                <div className='editor-btns'>

                    <Button type="primary" icon={posting ? <LoadIcon /> : <UploadIcon />}

                        disabled={text === undefined || text.length === 0 || posting || readonly}
                        //enabledOnReadOnly 

                        onClick={post}>

                        {t(postLabel ?? 'message.editor_send')}

                    </Button>
                </div>

            </Fragment>
            :

            <Button type='primary' icon={<EditIcon />} iconPlacement='left' className='edit-btn' onClick={() => setVisible(true)}>
                {collapsedMessage || t('message.editor_expand')}
            </Button>


    }

    </div >



})


const emptyDynamics = { recipients: [], topics: [] }


const useEditorDynamics = (props: {

    visible: boolean

    dynamicTopicSelector?: DynamicsEditor

}) => {



    const { dynamicTopicSelector, visible } = props

    const TopicSelector = dynamicTopicSelector! ?? DefaultDynamicsEditor


    const [dynamics, setDynamics] = useState<MessageDynamics>(emptyDynamics)

    useEffect(() => {

        if (!visible)
            setDynamics(emptyDynamics)


    }, [visible, setDynamics])


    const EditorDynamics = useStable(() => <TopicSelector dynamics={dynamics} onChange={setDynamics} />)


    return { EditorDynamics, dynamics }



}