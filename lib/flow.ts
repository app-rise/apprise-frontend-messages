import { useMessageChannel } from './channel';
import { Flow, Message } from './model';
import { useMessageTopics } from './topic';
import { FlowMessage } from './model';


export const useMessageFlow = () => {


    const topics =  useMessageTopics()
    const channels = useMessageChannel()

    const self = {

        refOf: (flow:Flow) => `${flow.name}:${flow.channels.map(({name,channel}) => `${name}:${channels.refOf(channel)}`)}`

        ,

        matches: (flow: Flow | undefined, m: Message) => flow?.channels.some(c => channels.matches(m, c.channel))

        ,

        intern: (flow: Flow, m: Message): FlowMessage =>( { ...m, flow, channel: flow.channels.map(c => c.channel).find(c => channels.matches(m, c))! })

        ,

        subtopics: (m: FlowMessage) => topics.partitionTopics(m.topics, m.channel.read).extra
    
    }

    return self;

}