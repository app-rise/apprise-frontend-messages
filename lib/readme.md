# Apprise Frontend Messages

`apprise` frontend support for in-app message exchange.

Requires these `apprise` dependencies at the frontend:

- `apprise-ui`
- `apprise-frontend-core`
- `apprise-frontend-events`
  
and `apprise-backend-messages` at the backend.

## Motivations

A system of embedded messages can offer some advantages over external solutions.

**It can contextualise messages to domain objects.**
  
Users don't need to browse messages by topic, channel, or sender.

They can navigate to domain objects and find there all the messages *about* those objects.

Messages are just another type of data in a one-to-many relationship with domain objects. 

Put differently, the objects *are* the topics.

**It can split message streams across the one-to-many relationships in the domain.**

Users can navigate to a *parent* object and find that its message stream includes also the streams of its *child* objects, where children are *parts*, *members*, *scopes*, etc.

**It can share message streams within the *party* structures in the domain.**

Messags can be exchanged among *groups*, *teams*, *tenants*, etc..

Party members may share a single identity, or retain their identity but respond on behalf of the other members.

Messages of different parties remain separate. 

**It can integrate messages with domain events.**

Object lifecycle events and other domain events like deadlines, can be notified as messages sent 'by the system’ to all relevant parties and streams. 

System messages can be multi-lingual and adapt to the language of the logged user.

This effectively consolidates user comunication and key log entries in a single stream of historical data.
 
**It can integrate messages with mail notifications**

Some or all messages can be echoed with emails so to reach and alert disconnected users.

Mails can link back directly into message streams of the application.

## Key Concepts

**A message *may* have an `author`.**
  
If it doesn't, it comes from the *system*. 

If it's not a *system message*, then it's a *user message*.

Authors are plain strings, we don't assume anything about them.  In most apps, they are user identifiers from `apprise-frontend-iam`.
  
**The author *may* be a member of a `party`.** 

If it doesn't, the author is its own party. 

This is always true of system messages. If it's true all user messages too, the messaging system is conventionally "user-level".

If user messages do have a party, the messaging system is "party-level": users post and process messages on behalf of their party, but authorship remains explicit.

In some apps, different users may even map onto the same author. Messaging is still party-level, but with a twist: all users write on behalf of each other, authorship remains hidden. 

Parties can be anything of relevance to the application. Commonly, they are the tenants of `apprise-frontend-iam`. 

But they can be any other group-like concept, including dynamic ones that exist only in some parts for the app for as long as users remains in there.

**A message has a `date`.**

So we can tell which messages come before other and show them ordered from the most recent to the least recent.

**A message has some `content`.**

User messages have text content. System messages have multi-lingual text content, so presentation can be tuned to the language of the user.

Multi-lingual content can be translated with the `intl` facilities of `apprise-frontend-core`.

**The content of a message is about one or more named `topics`.**

Like authors, topics are plain strings, but we namespace them as *(name,type)* pairs.

It's up to the application to define what are the topics of interest.

Typically, they use object identifiers as topic names and object types as topic types. 

Eg. messages can be about some (`submission`, `SUB-32432XD`) or about (`campaign`, `C-Y555GHR`) or about (`tenant`, `T-XDE38KL`).


**A message *may* have one or more real-time `recipients`.**

A recipient identifies an *event topic* in `apprise-frontend-events`.

A message with no specific recipient is pushed in real-time to every connected user. It's a broadcast. 

A message with multiple recipients is pushed in real-time to each recipient.

*The important gotcha is that recipients affect only real-time delivery*, only message topics affect the visibility of messages (see channels, flows, and boards below). Applications that want to enforce visibility rules may want to coordinate message topics and recipients so that messages that aren't to show, aren't pushed either.

It's up to the application to define what are the possible recipients, and to configure the subsystem to include them in messages and to listen on them. 

A common choice is to use the tenants of `apprise-frontend-iam`, plus an extra recipient for the group of tenant-less users, the *admins*.  Tenant users typically subscribe only to messages addressed to their own tenant, while admins subscribe to all messages, and maybe to messages dedicated to their own group.



**A message *may* have a `scope`.**

This is an opaque identifier that provides a coarse grouping of message for optimisation purposes.

`apprise-backend-messages` uses it to narrow its queries for messages.

If they use a scope at all, applications typically use the identifiers of some top-level context-like objects.

**A message may reply to another message.**

When it does, the message embeds a `reference` to the other message, a stub of the information in it, so that the first can be understood in the context of the author, party, and content of the second.

**A message tracks the list of parties that have read it.**

This tells the user if a message has been already processed by its own party. 

A message is implicitly read by the party of its own author, and replying to messages automatically marks them as read. 

Otherwise users need to be explicitly mark messages as read.

**A `channel` describes messages with the same topics.** 

These are the `read topics` of the channel. 

A message is in a channel if it has all its read topics.

**A channel has a `mode` to match messages.**

A channel with a `lax` mode matches all messages that have *at least* its read topics, though they can have *extra topics*.

A channel with a `strict` mode doesn't tolerate extra topics, it matches all messages that have *exactly* its read topics, no more and no less.

**A channel *may* also describe new matching messages, such as new posts and/or replies.**

This means the channel may provide *directives* for how to build such messages.

It may provide directives only for new posts, or only for replies.

A channel that describes new posts is a `write channel`. Otherwise it's a *readonly channel*.
  
**A write channel *may* define one or more `write topics` for new posts.**

If it doesn't, new posts have all the read topics.

Replies have always the topics of the messages they reply to.

**A `flow` describes messages in multiple channels.**

Each channel describes a homogeneous subset of the messages, but the flow can describe heterogeneous messages.

**A flow has a name.**

The name serves as an identifier, so that the system can tell two flows apart and manage collections of flows.

**A flow has a scope and one or more recipients.**

This is the scope and recipients of all the messages that are outgoing from some channel in the flow.

This scope propagages to all the messages outgoing from some channel in the flow.

**Channels and flows are frontend constructs, and aren't saved at the backend.**

Typically, they're statically defined. Dynamic flows aren't excluded, however.

**A message `board` is where users go to see and send messages in a given flow.**

The board shows only messages that match at least one channel in the flow.

It interacts with `apprise-backend-messages` to fetch, read, and post messages, and it reacts to push notifications to add new messages that were posted after the board was first rendered.

**A message `zone` is a set of app-specific configuration for the subsystem.**

Configuration includes:

-  the author and party of outgoing messages.
-  the recipients of messages to subscribe for.
-  a `message resolver` with message rendering customisations (names, labels, avatars, etc.).
-  `topic resolvers` with topic rendering customisations (names, labels, etc.).
-  `route resolvers` with the board routes that match incoming message notifications.


## Provider

**`<Messages>`** provides a set of global contexts for the library. 

Typically applications mount it high-up in the hierarchy, just below its dependencies:

 ```html
 <Application ...>
    <UserInterface ...>
        <Configuration ...>
            <Intl ...>
                <Client ...>
                    <Events ...>
                        <Messages ...>  <---------------
                            ...
                        </Messages>
                    </Events>
                </Client>
            <Intl>
        <Configuration>
    </UserInterface>
</Application>
 ```


`<Messages>`  mounts:

- a **`<MessageColourContext>`** to track associations of random colours to `authoringParty`s across all boards.
- a **`<MessageBoardContext>`** to keep track of open boards and unread message counts for each board.
- a global **`<MessageZone>`**.

If the application doesn't override it (see below), the global `MessageZone` is configured with:

- the `messageResolver` returned by **`useDefaultMessageResolverFactory()`**.
- the catch-all **`anyRecipient`** used to subscribe for *all* messages.

The default resolver:

- defaults the **`author`** of a message to **`system`**.
- defaults the **`authorName`** to the `author`, or to a specific name for the `system` default.
- defaults the **`authoringParty`** to the `author`.
- defaults the **`authoringPartyName`** to the `authoringParty`, if defined, or else to the `authorName`.
- returns a **`<DefaultAvatar>`**.

The default message resolver also usea the `authoringParty` to determine:

- if the message has been **`read`** by some member.
- if it's an **`own`** message, i.e. originates from some member.


## Configuration

Most applications will configure the global `<MessageZone>` mounted by `<Messages>`.

Some applications may need different configurations for different boards, however. They will configure multiple `<MessageZone>`s accordingly.

In all cases, there are multiple approaches to configuration:
   
1. pass it as properties to `<Messages>` or `<MessageZone>`.
2. pass it as properties to a **`<ZoneInitialiser>`** mounted below `<Messages>` or a `<MessageZone>`.
3. call **`useMessageZone()`** below `<Messages>` or `<MessageZone>` and invoke its registering methods.

The first method is the simplest to use. 

The second method may be necessary if resolvers need to read contexts that, for some reason, must be mounted below the `<MessageZone>`.

The third method may be preferred if the application makes already use of its own centralised initialiser. 

> but even so, the application initialiser may be simplified if it mounts `<ZoneInitialiser>` instead of invoking registration methods directly.

A `<MessageZone>` serves as a provider and mounts:

- a `<MessageAuthoringContext>` to hold the `author` and `authoringParty`, if any are passed in.
- a `<MessageZoneContext>` to hold the resolvers that may be passed in.

More precisely, the properties of `<Messages>` and `<MessageZone>` don't take resolvers directly, but *factories* that return resolvers: a **`messageResolverFactory`**, a **`topicResolverFactory`**, and a **`messageRouteFactory`**.

This let applications to implement factories as hooks that read from contexts and/or use other React features before returning resolvers as plain functions.  In particular, factories can access the `author` and `authoringParty` in the `<MessageAuthoringContext>` mounted by the `<MessageZone>`:

  - either directly, using **`useAuthorInfo()`** the utility.
  - or indirectly, by calling the `useDefaultMessageResolverFactory()` in order to extend it. 

> this is possible because a `<MessageZone>` mounts a `<ZoneInitialiser>` below the `<MessageAuthoringContext>` and delegates the actual configuraton to it.

Like other properties, custom factories are optional. The system:

- uses the `useDefaultMessageResolverFactory()` if a custom one isn't provided.
- uses topic identifiers instead of names or labels.
- doesn't notify users when messages arrive (as it doesn;t know where to take them). 

Finally, an application may also combine multiple methods for different pieces of the configuration. 

> for example, the `author` and the `authoringParty`  may be set on `<Messages>` and resolver factories may may set on a `<ZoneInitialiser>` below.


## Push Notifications

Posted messages are sent to `apprise-backend-messages`, where they're stored. 

From there, they are published to the Event Bus provided by `apprise-backend-events` under specific *event topics*.

The even topics are derived from `recipient` of messages.

In particular:

- a message with no `recipient` is published to the event topic **`any.message`**. 
- otherwise the `recipient` is split in segments separated by colons (`:`) and, for each segment `rec`, the message is published to to the events topic **`<rec>.message`**.


Applications set the intended `recipient`s on flows and attach flows to message boards. Messages posted from those boards  will then include the recipients. 

If they don't, messages are sent to all users.

A common pattern for applications is to use parties as recipients.

This means massages can be addressed to users of individual parties or groups of parties, or even broadcasted to users of all parties from specific boards. 

Applications where parties are tenants from `apprise-frontend-iam`, may also designate a special party for tenant-less users, e.g. use `*` as the `recipient`. 

This means messages posted from given boards may be restricted to tenant-less users, for administrative purposes.

Next, applications may want configure the `recipient` of messages they want the user be notified of. 

They can:

- set them as the `recipients` property of `Messages` or a `<MessageZone>`.
- set them as the `recipients` property of a `MessageSubscriber` below `Messages` or a `<MessageZone>`.
- do nothing, so that the users receives all messages.


The first case is a convenience over the second, as `<MessageZone>`s  mounts a `MessageSubscriber` internally and delegates the setup to it.  Applications may use the second approach if the `recipients` are determined based on information only available after `<Messages>` or the `MessageZone` has been mounted.

> A `<MessageSubscriber>` defaults its `recipients` to the `anyRecipient`, i.e. listens to all messages.
 
In all cases, push notifications are always processed by a `MessageSubscriber` . 

The subscriber can handle two types of events, new posts and message updates. 

> currently, the only update is when the message has been read by some party.

Both events carry full messages, and the subscriber and calls upon **`useBoardStore()`** to dispatch them.

For new posts, the store finds in `<MessageBoardContext>` all the board that match the message and:

1. are presently mounted. 
2. or have been previously mounted. 

In the first case, the store keeps the message in the `<MessageBoardContext>` for that board, so that the board may come and collect it. 

Even if the board is no longer mounted, its unread count may still show and need updating. So the store refreshes the unread count and adds the it to the context. 

At this point, the new message is delivered but the user may be far from any board that may display.

So the store calls on `useMessageZone()` to get the configured route resolvers, and ask each of them to produce a route to a board. 

If there are no resolvers, or no resolver can produce a route, the store is done. Otherwise it sends a toast notification with a button that the user can click to navigate to a board and see the message.

Finally, if the event is about a message update, the store refreshes the unread count of any matching board, whether currently mounted or not.

For matching boards that are currently mounted, it also replaces any notification about the same message that may have been previously received.

In any case, it operates silently and doesn't notify the user.

## Boards


A **`<MessageBoard>`** displays all the messages of a given **`Flow`** in a *backword-scrollable* list.

On mount, it calls upon `useBoardStore()` to retrieves a first batch of messages from `apprise-backend-messages`. 

For this, store passes over the wire as a qwuery the read topics and match mode for each of the flow's channels.

Then, as the user scrolls to the top of the list, it uses the *cursor* returned by the backend to request other batches from the same resultset .

Also on mount, the board calls on the store to register with the `<BoardContext>`, so as to be notified when new matching messsages comne in via push notifications and should be appended to the start of the list.

Similarly, when it is unmounted, the board asks the store to unregister it from the `<BoardContext>`.

The board delegates message rendering to a **`<BoardMessage>`**. 

A render includes all the metadata and content for the message, including topics, author, authoring party, read status, and reference. Topics are show only if they're extra topics, hence only if the the metching channel is `lax`. 

Applications can  the render through the message resolver that they have configured. 

The **`<BoardMessage>`**, also includes renders buttons for actions on the messsage, such as to read/unread the message and to reply to it. 

The board embeds an **`<BoardEditor>`** to write new posts and replies, provided the underlying flow has at least a channel that allows it.

The editor may optionally take a `DynamicsEditor`, an extension that lets users control some dynamic aspects of the outgoing messages.

Currently the protocol allows only recipients and topics as *message dynamics*.

Typically, a `DynamicsEditor` is a selector whereby users can restrict the recipients of "broader" boards. For example, outgoing messages would be normally broadcasted to all parties, but the selector can limit the recipients to a selected few.

As usual, changing the recipients may and normally will require an adjustment in topics, so that unselected parties won't receive the messages.

Applications can pass an `onPost` callback into the board for post-processing outgoing messages.

This is a point for integration with an email subsystem, applications can opt out for all or some messages.


Boards have other configuration points. They may:

- *may* be configured with the number of messages to fetch when the user scrolls up (`batchSize`).
- *may* be configured with a custom message filter (`filter`), or a custom topic filter (`topicFilter`).
- *may* be configured to start with the `<BoardEditor>` hidden (`collapsed`) behind a button with a given message (`collapsedMessage`).
