import { Topic } from './model';
import { useMessageZone } from './zone';



export const useMessageTopics = () => {

    const registry = useMessageZone()

    const self = {


        refOf: (t: Topic) => `[${t.type}:${t.name}]`

        ,

        // splits a set of topics depending on whetehr they match one of another set of topics.
        partitionTopics: (topics: Topic[], other: Topic[]) => {

            let matched = [] as Topic[]
            let extra = [] as Topic[]

            topics.forEach(t => (other.some(ot => ot.name === t.name && ot.type === t.type) ? matched : extra).push(t))

            return { matched, extra }

        }

        ,

        resolveTopic: (topic: Topic) => registry.lookupTopicResolver(topic.type)?.resolve(topic)


    }

    return self;

}