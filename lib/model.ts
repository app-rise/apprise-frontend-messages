import { Multilang } from 'apprise-frontend-core/intl/multilang'
import { LabelProps } from 'apprise-ui/label/label'
import { FC, ReactNode } from 'react'



export type Message = {

    id: string                      // internal.

    content: MessageContent

    topics: Topic[]                 // what the contant is about.        

    date: string                    // for ordering in UIs.

    author?: string                 // who authored the message. if missing, it's the system.

    authoringParty?: string         // the party of the author. if missing, the author itself.

    readBy: string[]                // parties that have "read" the message.

    replies?: MessageReference      // summary of the message, this replies to.ng me

    recipient?: string              // the target for push delivery.

    scope?: string                  // broad grouping for query partioning.

}

export type Topic = {

    name: string            // used for matching.

    type: string            // used for namespacing.

}


//  content can be a plain string (typically user-to-user) 
//or multilang, to adapt to the language of the user (tyopically system-to-user) 

export type MessageContent = string | MultilangContent




// multilang messages are translatable with intl facilities.
// 1. keys identify the text with decreasing priority.
// 2. parameters are injected into the text. paramerers can be
// 1. static strings (eg. user provided data).
// 2. in multilang objects to destructure based on the language of the user (eg. model data).
// 3. translable as parameters of the translation function (wuth their own keys, params, options, etc.)

export type MultilangContent = {

    keys: string[]
    parameters: Record<string, string | Multilang | TFunctionParameters>

}


// the parameters of the translation functions, very loosely typed.

export type TFunctionParameters = Record<string, any>



// an extract with the core-properties of a message. 

export type MessageReference = Pick<Message, 'id' | 'author' | 'authoringParty' | 'content'>



// returns type infomation about a topic of a given type

export type TopicResolver = {

    type: string
    resolve: (_: Topic) => TopicInfo | undefined         // undefined if it cannot resolve the message
}

// information to render a topic (typically in the context of displaying the message).
export type TopicInfo = {

    label: ReactNode
    tip?: ReactNode
    link?: string
}


// information to route a message (typically to its most specific message flow).
export type RouteInfo = {

    route: string

}

//  yields information to route a message.
//  may look at the entire message (typically its topics) to decide if it can.
//  if it can, it may need to lookup information in order to build the route, and lookup may fail.
//  so it may still not be able to return a route in practice.
export type RouteResolver = {

    // undefined if it doen't now how to route the message, or it knows but it cannot in practice.
    routeFor: (_: Message) => Promise<RouteInfo | undefined> | undefined
}




// stream of messages about given topics.
export type Channel = {

    read: Topic[]                   // messages must match.

    mode?: ChannelMode              // match mode for read topics: at least all (lax) vs. exactly all (strict).

    write?: Topic[]                 // topics on outgoing posts. by default, new posts have the read topics, replies have the original ones.

    noPost?: boolean                // disallows new posts.
    noReply?: boolean               // disallows replying.



    onReply?: (message_: Message) => Message

    // mailProfile?: (message: Message) => FlowMailProfile


}

export type ChannelMode = 'lax' | 'strict'


// names and aggregates multiple channels to capture different rules for inbound and outboung messages.
export type Flow = {

    name: string                       // used for logging            

    scope?: string                     // matches message scope in backend broad queries.

    recipient?: string                 // the default recipient of outgoing posts.

    channels: {

        name: string
        channel: Channel

    }[]

    //mailProfile?: (message: Message) => FlowMailProfile



}

// bridges between message properties and email properties.
// export type FlowMailProfile = {

//     targets: string[]               // who should the message be mailed to.
//     parameters: TParams            // what parameters should the mail message contain.
// }





// message properties that can be set per message
export type MessageDynamics = {

    recipients: string[],
    topics: Topic[]
}



// infomation that can be derived from a message
export type MessageInfo = Message & {

    Avatar: FC<AvatarProps>

    author: string
    authorName: string
    AuthorLabel: FC<LabelProps>
    authoringPartyName: string
    authoringParty:string

    own: boolean
    read: boolean
   
}

export type AvatarProps = {

    info: Omit<MessageInfo, 'Avatar'>
}


// a message augmented with properties from the board context in wnhich it's shown.
export type FlowMessage = Message & {

    flow: Flow
    channel: Channel                // the channel of a flow the message was matched to.
 
}

export type MessageResolver = (m: FlowMessage) => MessageInfo
