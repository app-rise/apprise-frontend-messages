import { useClientSession } from 'apprise-frontend-core/client/session'
import { mocks } from 'apprise-frontend-core/utils/mock'
import { useContext, useEffect, useState } from 'react'
import { MessageAuthoringContext, MessageColourContext } from './context'
import { Flow } from './model'
import { useBoardStore } from './store'

export const useAuthorInfo = () => {

    const session = useClientSession()

    const state = useContext(MessageAuthoringContext)


    const self = {

        author: () => state.get().author ?? session.get() ?? 'none',

        party: () => state.get().authoringParty ?? self.author()

    }


    return self

}

export const useMessageColours = () => {

    const colours = useContext(MessageColourContext)


    const colourOf = (id: string) => {

        if (!colours[id])
            colours[id] = mocks.randomColor()

        return colours[id]

    }

    return { colourOf }
}


export const useFlowCount = (flow: Flow, name?: string) => {

    const [count, setCount] = useState(0)

    const store = useBoardStore()

    // marking messages read/unread and also message notifications will trigger changes to the local flowstate.

    useEffect(() => {

        store.unreadCount(flow, name).then(setCount)

        // eslint-disable-next-line
    }, [store])


    return count;
}