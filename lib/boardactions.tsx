import { useT } from 'apprise-frontend-core/intl/language'
import { Button } from 'apprise-ui/button/button'
import { useAsyncRunner } from 'apprise-ui/utils/asyncrunner'
import { Flow } from './model'
import { useBoardStore } from './store'
import { useFlowCount } from './utils'


export const ReadAll = (props: {

    flow: Flow


}) => {


    const t = useT()

    const { flow } = props

    const unreadCount = useFlowCount(flow)

    const { waitFor, PulsingDotIcon } = useAsyncRunner()

    const store = useBoardStore()


    return <Button noReadonly disabled={!unreadCount} iconPlacement='left' icon={<PulsingDotIcon />} size='small' type='ghost' onClick={() => waitFor(store.markAllRead)(flow)}>
        {t('message.action_read_all')}
    </Button>
}