import { useT } from 'apprise-frontend-core/intl/language'
import { useL, useMultilang } from 'apprise-frontend-core/intl/multilang'
import { utils } from 'apprise-frontend-core/utils/common'
import { Message, MessageReference, MultilangContent, Topic } from './model'
import { useAuthorInfo } from './utils'



export const useMessage = () => {

    const t = useT()
    const l = useL()
    const ml = useMultilang()

    const authoring = useAuthorInfo()

    const self = {

        makeNew: (content: string, topics: Topic[], author?: string): Message => {

            return {

                id: undefined!,
                date: undefined!,
                author,  // no author => system message.
                authoringParty: authoring.party(),
                content,
                topics,
                readBy: []
            }

        }

        ,

        contentOf: (m: Message) => {

            const translatedParams = (content: MultilangContent) => Object.entries(content.parameters ?? {}).reduce((acc, [name, val]) => {

                if (typeof val === 'string')
                    return ({ ...acc, [name]: val })

                const { key, params = {}, options = {} } = ml.test(val) ? { key: l(val) } : val

                return ({ ...acc, [name]: t(key, { ...options, ...params }) })

            })

            return typeof m.content === 'string' ? m.content : t(m.content.keys, translatedParams(m.content))



        }

        ,

        refOf : (m: Message | undefined): MessageReference | undefined => m ? ({ id: m.id, author: m.author, authoringParty: m.authoringParty, content: m.content }) : undefined
 


        ,


        // id encodes date and author.
        comparator: (m1: Message, m2: Message) => utils().compareDates(m1.date, m2.date)


    }

    return self

}

