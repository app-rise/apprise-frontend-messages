import react from '@vitejs/plugin-react'
import { readdirSync } from 'fs'
import { defineConfig } from 'vite'
import checker from 'vite-plugin-checker'

const libs = `${__dirname}/src/lib/`

const proxy = { target: 'https://localhost:8443', changeOrigin: true, secure: false, headers: { "Connection": "keep-alive" } }
const wsproxy = { target: proxy.target, ws: true, changeOrigin: true, secure: false }



export default defineConfig({

  base: '/lab/',

  server: {
    port: 3000,
    proxy: {
      '/domain': proxy,
      '/event': wsproxy
    },
  },

  resolve: {
    preserveSymlinks: true,
    alias: [ 
      
      { find: /^#(.*)$/, replacement: `${__dirname}/src/$1`},
      ...readdirSync(libs).map(name => ({ find: name, replacement: `${libs}/${name}` }))
    
    ]

  },

  plugins: [

    react(),
    
    checker({
      overlay:{
        initialIsOpen:false
      },
      typescript: true,
      eslint: {
        lintCommand: 'eslint "src/**/*{.ts,.tsx}"'
      }
    })
  ]
})
