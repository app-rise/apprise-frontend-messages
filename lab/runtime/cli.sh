devruntime=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
appname=lab
ns=lab
data=${APPRISE_DATA:-"$devruntime"/data}
mode=${APPRISE_MODE:-"host"}
clustername="kind-2305"
kubeversion="v1.26.4"

home="$(git rev-parse --show-toplevel)/lab"

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

# super simple completion for app
_app_complete() {
    local cur prev

    cur=${COMP_WORDS[COMP_CWORD]}
    prev=${COMP_WORDS[COMP_CWORD-1]}

    case ${COMP_CWORD} in
        1)
            COMPREPLY=($(compgen -W "up down pull" -- ${cur}))
            ;;
        2)
            case ${prev} in
                pull)
                    COMPREPLY=($(compgen -W "domain" -- ${cur}))
                    ;;
				up)
                    COMPREPLY=($(compgen -W "backend gateway domain events db" -- ${cur}))
                    ;;
            esac
            ;;
        *)
            COMPREPLY=()
            ;;
    esac
}

_cluster_complete() {
    local cur prev

    cur=${COMP_WORDS[COMP_CWORD]}
    prev=${COMP_WORDS[COMP_CWORD-1]}

    case ${COMP_CWORD} in
        1)
            COMPREPLY=($(compgen -W "create delete status" -- ${cur}))
            ;;
        *)
            COMPREPLY=()
            ;;
    esac
}

complete -o default -F _app_complete app
complete -o default -F _cluster_complete cluster

source $devruntime/git-prompt.sh

PS1='\[$green\][${ns}]\[$reset\] \w> '

evalin() {
	local curr=$PWD
	cd $1 && shift
	eval $@
	cd $curr
}


dbName() {

	echo "$(kubectl get pod --namespace=${ns} --selector=svc=db -o jsonpath='{.items[*].metadata.name}')"
}


cluster() {
	 
	local cmd=${1:-"get"}; shift

	cluster="kindcluster"
	
	if [ $cmd = "create" ]
	then
		if [ "$($cluster status)" = "0" ]
		then
			$cluster create
		fi
			
		cluster status
		
		return
	fi

	if [ $cmd = "status" ]
	then
	   
	   local status=$($cluster status)

	   if [ "$status" = "0" ]
	   then
		 echo "${green}$($cluster name)${reset} is down"
	   else
	      echo "${green}$($cluster name)${reset} is up"
	   fi 
	   return
	fi

	if [ $cmd = "delete" ]
	then
	   
	   local status=$($cluster status)

	   if [ "$status" = "1" ]
	   then
		$cluster delete
	   fi 
	  
	    cluster status
		
		return
	fi

	if [ $cmd = "config" ]
	then
	   
	   local status=$($cluster status)

	   if [ "$status" = "1" ]
	   then
		$cluster config
	    else
			cluster status
	   fi 
	  
		return
	fi


	echo "${red}no, don't do '$cmd'.${reset}" && return 1
}

kindcluster() {

	local cmd=${1:-"name"}; shift

	reg_name="${clustername}-registry"
	reg_port='5000'

	if [ $cmd = "name" ]
	then
	    echo "${clustername}"
		return
	fi

	if [ $cmd = "create" ]
	then


		# in 'dind' mode, push and pull use cluster dns.
		#in 'host' mode, push and pull use localhost. 
		local registryHost 

		if [ "$mode" = "dind" ]
		then
			registryHost=$reg_name
		else
			registryHost="localhost"
		fi

cat <<EOF | kind create cluster --name ${clustername} --image kindest/node:${kubeversion} --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."localhost:${reg_port}"]
    endpoint = ["http://${reg_name}:5000"]
nodes:
- role: control-plane
  extraMounts:
  - hostPath: $data
    containerPath: /data
EOF

		if [ "$mode"  = 'dind' ]
		then
			configlocation="/root/.kube/config"
			mkdir -p "${configlocation%/*}"
			kind get kubeconfig --name ${clustername} --internal > "$configlocation" 2>&1		
		fi


# Document the local registry
# https://github.com/kubernetes/enhancements/tree/master/keps/sig-cluster-lifecycle/generic/1755-communicating-a-local-registry
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: local-registry-hosting
  namespace: kube-public
data:
  localRegistryHosting.v1: |
    host: "localhost:${reg_port}"
    help: "https://kind.sigs.k8s.io/docs/user/local-registry/"
EOF

		if [ "$mode"  = 'dind' ]
		then
			configlocation="/root/.kube/config"
			mkdir -p "${configlocation%/*}"
			kind get kubeconfig --name ${clustername} --internal > "$configlocation" 2>&1		
		fi

		return
	fi

	if [ $cmd = "status" ]
	then

		kind get kubeconfig --name ${clustername} > /dev/null 2>&1

		if [ $? = "1" ]
		then 
			echo 0	
		else

			# creates local registry node if it doesn't exist
			local registryRunning="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)" 

			if [ "$registryRunning" != 'true'  ]
			then
			    echo "create reg"
				docker run -d --restart=always -p "127.0.0.1:${reg_port}:5000" --name ${reg_name} registry:2  >/dev/null

			fi

			# connect the registry to the cluster network
			# (the network may already be connected)
			docker network connect kind ${reg_name} > /dev/null 2>&1

		    echo 1
		fi
		return
	fi


	if [ $cmd = "delete" ]
	then

		kind delete cluster --name ${clustername}

		# creates local registry node if it doesn't exist
		local registryRunning="$(docker inspect -f '{{.State.Running}}' "${reg_name}" 2>/dev/null || true)" 

		if [ "$registryRunning" == 'true'  ]
		then
			docker container rm ${reg_name} --force

		fi

		return
	fi

	if [ $cmd = "config" ]
	then

		kind get kubeconfig --name ${clustername}
		return
	fi

	echo "${red}no, don't do '$cmd'.${reset}" && return 1


}


app() {
	
	describe() {
		help="$help${green}\n$1${reset}\t$2"
	}


	describeEnv () {
		help="$help${yellow}\n$1${reset}\t$2"
	}

	local help="\n${appname^^} CLI\n=======================================================================================\n"

	
	describe "home" "$home"

	describe 
	describe "app up" "starts all services."
	describe "app up svc1 svc2 ..." "starts selected services."
	describe "app up backend" "starts all backend services."
	describe "app down" "stops all services, resets all data."
	describe "app pull <svc>" "downloads and deploys the latest versiom of <svc>."



	local cmd=${1:-"help"}; shift


	if [ $cmd = "help" ]
	then
		tabs -36 && echo -e "$help\n" && tabs -8
		return
	fi


	if [ $cmd = "up" ]; then
	
		echo "${green}starting up ${ns}...${reset}"

			# create cluster if not there 
		cluster create

		args=("$@")
		
		# recognises and expands multi-service keywords.
		if [ "${args[0]}" = "backend" ]
		then
		  args=(db gateway domain events"${args[@]:1}")
		fi

		evalin $devruntime tilt up "${args[@]}"

	   # test ns readiness by checking it has a default service account.
		printf "${green}waiting for namespace to be ready...\\r"
		n=0; until ((n >= 60)); do kubectl get serviceaccount default --namespace=${ns} -o name > /dev/null 2>&1  && break; n=$((n + 1)); sleep 1; done; ((n < 60)) 
		printf "waiting for namespace to be ready...done.${reset}\n"

	   return
	fi


	
	if [ $cmd = "down" ]; then
   	   
	   echo "${green}stopping ${ns}...${reset}" 
	   # undeploys anything originally deployed via Tilt
	   evalin $devruntime tilt down "$@" > /dev/null
	
	   return
	fi
	

	if [ $cmd = "pull" ]; then

	   local svc=$1; shift

	   if [ -z "$svc" ]
	   then
	    	echo "${red}it's: app pull <service>.${reset}" && return 1
	   fi

	   printf "${green}pulling latest ${svc}...\\r" && evalin $home mvn initialize -P${svc} -U "$@" && printf "${green}pulling latest ${svc}...done.${reset}\n"


	   return
	fi

	
}

app