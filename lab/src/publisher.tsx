import { testauthors, testrecipient, testtopics } from '#model';
import { TestSession } from 'apprise-frontend-core/client/session';
import { useT } from 'apprise-frontend-core/intl/language';
import { useForm } from 'apprise-frontend-core/utils/form';
import { mocks } from 'apprise-frontend-core/utils/mock';
import { useMessageCalls } from 'apprise-frontend-messages/calls';
import { useMessageChannel } from 'apprise-frontend-messages/channel';
import { MessageIcon, recipientSeparator } from 'apprise-frontend-messages/constants';
import { useMessage } from 'apprise-frontend-messages/message';
import { Flow } from 'apprise-frontend-messages/model';
import { useMessageTopics } from 'apprise-frontend-messages/topic';
import { useFlowCount } from 'apprise-frontend-messages/utils';
import { Button } from 'apprise-ui/button/button';
import { Form } from 'apprise-ui/form/form';
import { Label } from 'apprise-ui/label/label';
import { MultiSelectBox, SelectBox } from 'apprise-ui/selectbox/selectbox';
import { TextBox } from 'apprise-ui/textbox/textbox';
import { lorem, loremL, loremM } from 'apprise-ui/utils/constants';
import { useState } from 'react';



export const TestPublisher = (props: {

    flow: Flow

}) => {

    return <TestSession>
        <Inner {...props} />
    </TestSession>

}


const Inner = (props: {

    flow: Flow

}) => {

    const t = useT()

    const { flow } = props

    const calls = useMessageCalls()

    const messages = useMessage()
    const topics = useMessageTopics()
    const channels = useMessageChannel()

    const count = useFlowCount(flow)

    const [channel, setChannel] = useState(flow.channels[0])

    const initialMessage = messages.makeNew(mocks.randomIn([loremM, lorem, loremL]), testtopics, 'him')

    const [recipients, setRecipients] = useState<string[] | undefined>([testrecipient])

    const { edited, set, reset } = useForm(initialMessage);

    const send = async () => {

        await calls.postMessage({ ...edited, authoringParty: undefined, recipient: recipients?.join(recipientSeparator) || undefined })

        reset.to({ ...edited, content: mocks.randomIn([loremM, lorem, loremL]) }).quietly()
    }

    const removeAndReload = () => calls.removeMany({ scope: flow.scope! }).then(() => location.reload())

    return <Form>

        <SelectBox options={flow.channels} keyOf={c => c.name} label='channel' render={c => `${c.name} (${channels.refOf(c.channel)})`} onChange={c => setChannel(c!)}>
            {channel}
        </SelectBox>

        <SelectBox placeholderAsOption placeholder={t('message.system_author')} options={Object.keys(testauthors)} keyOf={t => t} label='author' render={c => <Label icon={<MessageIcon />} title={t(testauthors[c])} />} onChange={set.with((m, v) => m.author = v)}>
            {edited.author}
        </SelectBox>

        <TextBox multi={4} label='text' onChange={set.with((m, v) => m.content = v)}>
            {edited.content as string}
        </TextBox>

        <MultiSelectBox options={testtopics} label='topics' keyOf={topics.refOf} render={t => <Label noIcon title={topics.refOf(t)} />} onChange={set.with((m, v) => m.topics = v)}>
            {edited.topics}
        </MultiSelectBox>

        <MultiSelectBox placeholder="<any>" options={[testrecipient, "other"]} label='recipients' onChange={setRecipients}>
            {recipients}
        </MultiSelectBox>


        <div style={{ display: 'flex' }}>
            <Button type='primary' onClick={send}>Send</Button>
            <Button style={{ marginLeft: 10 }} type='danger' onClick={removeAndReload}>Reset</Button>
            <div style={{ marginLeft: 'auto' }}>Unread: {count}</div>
        </div>
    </Form>
}