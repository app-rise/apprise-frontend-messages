import { mocks } from 'apprise-frontend-core/utils/mock';
import { Fragment, PropsWithChildren, createContext, useContext, useState } from 'react';


export const App = () => {

    const random = mocks.randomText("random")

     return <Lib value="appval" func={dynamicfunc(random)}>
       <AppCompo />
    </Lib>
}


// const useAppHook = () => {
    
//     const ctx  = useContext(LibContext)

//      return ctx.value + "/" + mocks.randomText("random")
// }

// const useDynamicAppHook = (seed:string) => {
    
//     const ctx  = useContext(LibContext)

//      return () => ctx.value + "/" + seed
// }

const dynamicfunc = (seed:string) => {
    
    const useX = () => {

        const state = useState(0)

        const ctx  = useContext(LibContext)

        return ctx.value + "/" + seed + "/" + state[0]

    }

    return useX
}


const AppCompo = () => {

    const ctx  = useContext(LibContext)

    return <Fragment>
        {ctx.value}-{ctx.func()}
    </Fragment>
}

const LibContext = createContext({value: "init", func: () => "init" as string})

export const Lib = (props: PropsWithChildren<Partial<{

    value: string
    func: () => string

}>>) => {

    const { children,value="defval" , func=() => "deffunc"} = props

    return <LibContext.Provider value={{value, func}}>
        {children}
    </LibContext.Provider>
}