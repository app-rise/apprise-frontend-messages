import { Channel, Flow, Topic } from 'apprise-frontend-messages/model';


export const customAuthor = 'me'
export const customAuthoringParty = 'Family'

export const testrecipient = "test"


export const testauthors = {

    'me': 'Me',
    'you': `You`,
    'him': 'Him',
    'them': 'Them'

}

export const testtopicOne: Topic = { type: "type1", name: "One" }
export const testtopicTwo: Topic = { type: "type1", name: "Two" }
export const testtopicThree: Topic = { type: "type2", name: "Three" }
export const testtopicFour: Topic = { type: "type3", name: "Four" }
export const testtopicFive: Topic = { type: "type3", name: "Five" }
export const testtopicSix: Topic = { type: "type4", name: "Six" }

export const testtopics = [testtopicOne, testtopicTwo, testtopicThree, testtopicFour, testtopicFive,testtopicSix]

export const testChannelA: Channel = {

    mode: 'lax',
    read: [testtopicOne, testtopicThree]

}

export const testChannelB: Channel = {

    mode: 'strict',
    read: [testtopicTwo]

}


export const testflow: Flow = {

    name: 'test',
    recipient: testrecipient,
  
    channels: [

        {
            name: "channel A",
            channel: testChannelA
        }
        ,


        {
            name: "channel B",
            channel: testChannelB
        }

    ]
}
