


import { useTopicResolvers } from '#config';
import { useRenderGuard } from 'apprise-frontend-core/utils/renderguard';
import { useMessageZone } from 'apprise-frontend-messages/zone';
import { PropsWithChildren } from 'react';




export const CustomInitialiser = (props: PropsWithChildren) => {

    const { children } = props

    const resolvers = useTopicResolvers()

    const registry = useMessageZone()

    const { content } = useRenderGuard({

        render: children,
        orRun: async () => registry.registerTopicResolvers(resolvers)
    })


    return content
}

