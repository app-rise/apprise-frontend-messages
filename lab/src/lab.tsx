

import { useMessageResolver, useTopicResolvers } from '#config';
import { customAuthor, customAuthoringParty, testflow, testrecipient } from '#model';
import { TestPublisher } from '#publisher';
import { Application } from 'apprise-frontend-core/application';
import { Client } from 'apprise-frontend-core/client/client';
import { Configuration } from 'apprise-frontend-core/config/configuration';
import { Intl } from 'apprise-frontend-core/intl/intl';
import { vite } from 'apprise-frontend-core/utils/vitebridge';
import { Events } from 'apprise-frontend-events/events';
import { MessageBoard } from 'apprise-frontend-messages/board';
import { ReadAll } from 'apprise-frontend-messages/boardactions';
import { MessageIcon } from 'apprise-frontend-messages/constants';
import { Messages } from 'apprise-frontend-messages/messages';
import { Page } from 'apprise-ui/page/page';
import { UserInterface } from 'apprise-ui/provider';
import { ReadOnly } from 'apprise-ui/readonly/readonly';
import { Scaffold, Section } from 'apprise-ui/scaffold/scaffold';
import { DotIcon } from 'apprise-ui/utils/icons';
import { Todo } from 'apprise-ui/utils/todo';
import { RiHome7Line } from 'react-icons/ri';


const config = {
    client: {
        services: { domain: { label: "domain", prefix: "/domain", default: true } }
    }
}

export const MessageLab = () => {


    return <Application tool={vite}>
        <UserInterface>
            <Configuration config={config}>
                <Intl>
                    <Client>
                        <Events>
                            <Messages author={customAuthor}
                                authoringParty={customAuthoringParty}
                                messageResolverFactory={useMessageResolver}
                                topicResolverFactory={useTopicResolvers}
                                recipients={[testrecipient]}>
                                <Scaffold icon={<MessageIcon />} sidebar title={'Apprise Messages Lab'}>

                                    <Section title='home' icon={<RiHome7Line />} route="/" exact>
                                        <Page>
                                            <div style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                                                {/* <TestPublisher flow={testflow} /> */}
                                                <br /><br /><br />
                                                <div style={{ flexGrow: 1 }}>
                                                    <ReadOnly value={false}>
                                                        <MessageBoard
                                                            flow={testflow}
                                                            actions={[<ReadAll flow={testflow} />]}
                                                            filterTopics={topic => topic.name?.toLowerCase() !== 'five'} />
                                                    </ReadOnly>
                                                </div>
                                            </div>
                                        </Page>
                                    </Section>

                                    <Section icon={<DotIcon />} title='other' route="/other">
                                        <Page>
                                            <Todo />
                                        </Page>
                                    </Section>

                                </Scaffold>
                            </Messages>
                        </Events>
                    </Client>
                </Intl>
            </Configuration>
        </UserInterface>
    </Application>

}