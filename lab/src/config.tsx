import { useDefaultMessageResolverFactory } from 'apprise-frontend-messages/defaults';
import { FlowMessage, MessageResolver } from 'apprise-frontend-messages/model';



import { MessageIcon } from 'apprise-frontend-messages/constants';
import { TopicResolver } from 'apprise-frontend-messages/model';
import { Label } from 'apprise-ui/label/label';

export const useMessageResolver = () : MessageResolver => {


    const defaultProducer = useDefaultMessageResolverFactory()

    return (m: FlowMessage) => {

        const defaultInfo = defaultProducer(m)

        const authorName = m.author === "me" ? "Me Myself" : m.author === "you" ? "yourself" : defaultInfo.authorName

        return { ...defaultInfo, authorName }

    }
}


export const useTopicResolvers = (): TopicResolver[] => {

    return [
        {
            type: 'type1',
            resolve: t => ({ label: <Label icon={<MessageIcon color='coral' />} title={t.name} />, tip: `Topic ${t.name}`, link: "/other" })
        },
        {
            type: 'type2',
            resolve: t => ({ label: <Label icon={<MessageIcon color='lightseagreen' />} title={t.name} />, tip: `Topic ${t.name}`, link: "/other" })
        },
        {
            type: 'type3',
            resolve: t => ({ label: <Label icon={<MessageIcon color='cadetblue' />} title={t.name} />, tip: `Topic ${t.name}`, link: "/other" })
        },
        {
            type: 'type4',
            resolve: t => ({ label: <Label icon={<MessageIcon color='teal' />} title={t.name} />, tip: `Topic ${t.name}`, link: "/other" })
        }

    ]
}

export const useCustomTopicResolvers = (): TopicResolver[] => {

    const [, ...rest] = useTopicResolvers().reverse()

    return rest
}