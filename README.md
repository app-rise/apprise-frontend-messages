# Apprise Frontend Messages.

An `apprise` [fragment](https://gitlab.com/app-rise/apprise-tracker/-/issues/9) with application support for managing in-app messaging at the frontend.


## Getting Started

---

This is an `apprise` fragment and adopts this standard structure:

* `lib` contains the code.
* `resources` contains assets, like translations and images.
* `lab` contains a minimal `apprise` application that tests and showcases the code.

Instructions on how to run the `lab` are in `lab/README.md`.
